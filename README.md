Projekt für Textanalyse.

Funktionen:

Analyse:
- Gesamte Wörter zählen
- Verschiedene Wörter zählen
- Häufigkeit der verschiedenen Wörter
- Häufigkeit einzelner Buchstaben aus dem Text zählen
- Anzahl der Buchstaben mit und ohne Leerzeichen
- Textformatierung:
    - Zeilenumbrüche nach n Wörtern
    - Zeilenumbrüche nach n Buchstaben (es wird nach vollen Wörtern umgebrochen)

Command-line Interface:
- Argumente werden auf Gültigkeit geprüft
- Es wird zwischen Argumenten und Dateipfaden unterschieden
- Eine dynamische Hilfe wird generiert

Menü:
- Wird nur aufgerufen, wenn dem Programm keine Argumente übergeben werden
- Untermenüs wurden konfiguriert, um eine leichtere Bedienung zu gewährleisten
- Exit Menüpunkt und Eingabe wird automatisch generiert

Menubuilder:
Diese Klasse soll das erstellen von Menüs vereinfachen.
Dafür müssen nur die Einträge als String Array übergeben werden.
Die Auswahl wird als Integer Wert zurückgeliefert.
