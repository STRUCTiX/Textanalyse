package com.structix;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by structix on 14.05.16.
 */

/**
 * Analyse Klasse:
 * Hier wird sowohl die Datei eingelesen, als auch verarbeitet.
 * Die Ergebnisse werden dann von den Methoden zurückgeliefert
 *
 */

public class Analyse {
    //Definition der globalen Variablen
    private String inhalt = "";
    private String woerterGesamt[];
    private String einzelneWoerter[];

    //Konstruktor des Objekts mit dem Dateipfad als Übergabewert
    public Analyse(String dateipfad) {
        //Inhalt der Datei wird an den globalen String inhalt übergeben
        inhalt = dateilesen(dateipfad);
        //Text wird direkt für die Verarbeitung angepasst
        textAnpassen();

        //Initialisierung der globalen Variablen
        verschiedenWoerter();
        haeufigkeit();
    }

    //Hier wird die Datei aus einem bestimmten Dateipfad ausgelesen
    private String dateilesen(String dateipfad) {
        //Variablen werden definiert
        Scanner datei;
        String text = "";
        //Der try-catch Block wird benötigt, falls die Datei nicht vorhanden ist
        try {
            //Scanner bekommt den Dateipfad als File übergeben
            datei = new Scanner(new File(dateipfad));
            //Der Inhalt wird nach dem Regulären Ausdruck
            // \Z also bis zum Ende des Inputs bzw. bis zum letzten Terminator (falls vorhanden)
            //eingelesen. Ein weiterer \ kommt hinzu, um die Escapesequenz für einen \ einzuleiten.
            text = datei.useDelimiter("\\Z").next();
            //Scanner wird geschlossen
            datei.close();
        } catch (FileNotFoundException ex) {
            //Falls es zu einem Fehler kam, soll auf System.err eine kurze Fehlermeldung erscheinen.
            ex.getLocalizedMessage();
        }
        //Text wird als String zurückgegeben
        return text;
    }

    //Hier wird der Text angepasst
    public void textAnpassen() {
        String verarbeitung;
        //Leere Zeilen werden entfernt und Zeilenumbrüche mit Leerzeichen ersetzt
        verarbeitung = inhalt.replace("\n\n", "");
        verarbeitung = verarbeitung.replace("\n", " ");
        //Array des Textes wird mit dem Leerzeichen Delimiter erstellt
        woerterGesamt = verarbeitung.split(" ");
    }
    //Anzahl der Wörter werden zurückgegeben, da die Länge des Arrays aller Wörter abgefragt wird
    public int laenge() {
        return woerterGesamt.length;
    }


    //Hier werden die verschiedenen Wörter im Text bestimmt
    public int verschiedenWoerter() {
        //Initialisierung eines leeren temporären Arrays mit der länge von allen Wörtern wird erstellt
        String tempArray[] = new String[woerterGesamt.length];
        boolean vorhanden;
        int tempbelegt = 0; //Zähler für ein noch nicht vorhandenes Wort


        //Gesamte Wörter Liste wird durchgegangen
        for (int i = 0; i < woerterGesamt.length; i++) {
            vorhanden = false; //Wird vor jedem Durchlauf auf false gesetzt

            //Schleife um jeden Eintrag im temporären Array mit dem woerterGesamt Array zu vergleichen
            for (int z = 0; z < tempArray.length; z++) {
                //Wenn der Eintrag einmal oder mehrmals vorhanden ist wird vorhanden auf true gesetzt
                if (woerterGesamt[i].equals(tempArray[z])) {
                    vorhanden = true;
                }
            }
            //Wenn vorhanden auf false ist, also der Eintrag im temporären Array
            //noch nicht vorhanden ist, soll ein neuer Eintrag erstellt werden
            if (!vorhanden) {
                tempArray[tempbelegt] = woerterGesamt[i];
                //Zähler wird für die richtige Position um 1 inkrementiert
                tempbelegt++;
            }
        }

        //Das Array einzelneWoerter wird mit der Länge vondem temporär Zähler initialisiert
        einzelneWoerter = new String[tempbelegt];
        //Hier werden alle Werte von tempArray in das einzelneWoerter Array geschrieben
        for (int i = 0; i < tempbelegt; i++) {
            einzelneWoerter[i] = tempArray[i];

        }
        //Der Rückgabewert wieviele verschiedene Wörter es gibt
        return tempbelegt;
    }

    //Hier wird die Häufigkeit der einzelnen Wörter im Text bestimmt
    public String haeufigkeit() {
        //Ein leeres Integer Array für die Anzahl der einzelnenWoerter wird erstellt
        int anzahl[] = new int[einzelneWoerter.length];

        //Alle Einträge werden auf 0 gesetzt, damit danach hochgezählt werden kann
        for (int i = 0; i < anzahl.length; i++) {
            anzahl[i] = 0;
        }

        //Die gesamte Wörter Liste wird durchgegangen
        for (int i = 0; i < woerterGesamt.length; i++) {
            //Darin wird wiederum die einzelneWoerter Liste durchgegangen,
            //um die Anzahl an der richtigen Stelle zu inkrementieren
            for (int z = 0; z < einzelneWoerter.length; z++) {
                //Vergleich von einem Eintrag in der gesamten Wörterliste mit allen Einträgen
                //aus dem einzelneWoerter Array
                if (woerterGesamt[i].equals(einzelneWoerter[z])) {
                    anzahl[z] += 1;
                }
            }
        }
        //Hier wird die Methode sortieren aufgerufen und dort das sortierte Ergebnis ausgegeben
        return sortieren(einzelneWoerter, anzahl);
    }

    //Kompletter Text wird ausgegeben
    public String textausgabe() {
        return inhalt;
    }

    //Sortier Methode
    private String sortieren(String woerter[], int anz[]) {
        /**
         * Diese Methode basiert auf dem Bubblesort Sortieralgorithmus.
         * Dieser wurde jedoch um ein Array erweitert. Es wird nach absteigendenden Zahlen sortiert
         * und in diesem Verhältnis das woerter Array mit sortiert.
         * Das heißt die Anordnung der beiden Arrays ist nach dem sortieren immernoch korrekt.
         */

        //Temporäre Variablen für Integer und String
        String tempstring;
        int tempint;
        //Das komplette Array wird durchgegangen
        for (int i = 1; i < woerter.length; i++) {
            //Darin wird das selbe Array (selbe Länge) nochmals durchgegangen
            //Allerdings bei jedem Durchgang um i weniger, da diese Einträge schon sortiert wurden
            for (int z = 0; z < anz.length - i; z++) {
                //Wenn Eintrag an der aktuellen Stelle kleiner wie an der nächsten Stelle ist,
                //soll umsortiert (Plätze getauscht) werden
                if (anz[z] < anz[z + 1]) {
                    //Aktueller Eintrag wird ausgela-creditsgert
                    tempint = anz[z];

                    //Aktueller Eintrag wird mit dem nächsten Eintrag ersetzt
                    anz[z] = anz[z + 1];

                    //Nächster Wert wird mit dem ausgelagerten Wert überschrieben
                    anz[z + 1] = tempint;

                    //Das selbe Verfahren wird nun mit dem String gemacht,
                    //da die Positionen gleich bleiben sollen
                    tempstring = woerter[z];
                    woerter[z] = woerter[z + 1];
                    woerter[z + 1] = tempstring;
                }
            }
        }

        //Hier werden die sortierten Einträge nacheinander ausgegeben bzw. in den String ausgabe geschrieben
        String ausgabe = "";
        for (int i = 0; i < anz.length; i++) {
            ausgabe += woerter[i] + ": " + anz[i] + " (" + prozentsatz(anz[i], laenge()) + "%)" + "\n";
        }
        //Ausgabe wird zurückgeliefert
        return ausgabe;
    }

    //Hier wird die Anzahl der Buchstaben inklusive Leerzeichen zurückgegeben
    public int anzBuchstabenMLeerzeichen() {
        //Mit Leerzeichen
        return inhalt.length();
    }

    //Hier wird die Anzahl der Buchstaben ohne Leerzeichen ausgegeben
    public int anzBuchstabenOLeerzeichen() {
        //Ohne Leerzeichen
        String temp = inhalt.replace(" ", ""); // Leerzeichen wird gegen "nichts" ersetzt
        return temp.length();
    }

    //Methode zum errechnen des Prozentsatzes
    private double prozentsatz(int prozentwert, int grundwert) {
        return (double) prozentwert / grundwert * 100;
    }

    //Prozentualer Anteil der Leerzeichen
    public int stats_leerzeichen() {
        return (int) prozentsatz(anzBuchstabenMLeerzeichen() - anzBuchstabenOLeerzeichen(), anzBuchstabenMLeerzeichen());
    }



    public String textformatWoerter(int anzahlWoerter) {

        //Hier wird der Text anhand der Anzahl der Wörter umgebrochen

        String output = "";
        int zaehler = 0;

        for (int i = 0; i < woerterGesamt.length; i++) {

            /**
             * Solange der Zeilenlängen Zähler (hier zaehler genannt)
             * kleiner der Anzahl der Wörter ist, werden die Wörter nur in eine Zeile geschrieben.
             * Überschreitet der zaehler dieses Kriterium, wird ein Zeilenumbruch gemacht und der
             * zaehler auf 0 gesetzt.
             */

            if (zaehler < anzahlWoerter) {
                output += woerterGesamt[i] + " ";
                zaehler++;
            } else {
                output += "\n";
                zaehler = 0;
                //Da bei diesem Item der Umbruch gesetzt wurde,
                //muss es noch normal behandelt werden --> i dekrementieren
                i--;
            }
        }
        return output;
    }


    public String textFormatBuchstaben(int anzahlBuchstaben) {

        //Hier wird der Text nach Anzahl der Buchstaben umgebrochen (nur ganze Wörter)

        String output = "";

        if (laengstesWort() <= anzahlBuchstaben) {
            int zeile = 0;
            for (int i = 0; i < woerterGesamt.length; i++) {

                /**
                 * Es wird geprüft, ob die Zeile mit dem neuen Wort noch kleiner/gleich dem Maximum ist.
                 * Wenn ja, wird es der Zeile hinzugefügt.
                 * Wenn nicht, wird ein Zeilenumbruch eingefügt, die Buchstaben Anzahl der Zeile auf 0 gesetzt
                 * und das Wort wiederholt, damit es in die neue Zeile kommt und nicht übersprungen wird.
                 */

                zeile += woerterGesamt[i].length();
                if (zeile <= anzahlBuchstaben) {
                    output += woerterGesamt[i] + " ";
                } else {
                    output += "\n";
                    zeile = 0;
                    i--;
                }
            }
        } else {
            //Fehler
            output =  "Anzahl der Buchstaben muss mindestens dem längsten Wort entsprechen(" + laengstesWort() + " Zeichen).";
        }
        return output;
    }


    public int laengstesWort() {

        /**
         * Hier wird das längste Wort ermittelt, in dem der Variable max immer die momentan
         * gräßte Länge zugewiesen wird.
         */

        int max = 0;
        for (int i = 0; i < einzelneWoerter.length; i++) {
            if (einzelneWoerter[i].length() > max) {
                max = einzelneWoerter[i].length();
            }
        }
        return max;
    }



    public String hauefigkeitBuchstaben(String muster) {

        //Hier wird die Häufigkeit der Buchstaben bestimmt

        //Das Muster und der Inhalt werden in ein Char Array konvertiert. Damit können sie direkt verglichen werden
        char buchstaben[] = muster.toLowerCase().toCharArray();
        int anzahl[] = new int[buchstaben.length];
        char allebuchstaben[] = inhalt.toLowerCase().toCharArray();
        String output = "";
        //Anzahl Array mit 0 initialisieren
        for (int i = 0; i < anzahl.length; i++) {
            anzahl[i] = 0;
        }


        for (int i = 0; i < allebuchstaben.length; i++) {
            for (int z = 0; z < buchstaben.length; z++) {

                /**
                 * Wenn der Buchstabe des Inhalts an der richtigen Stelle des Musters gleich ist,
                 * wird die dieser Buchstabe im Anzahl Array um 1 inkrementiert.
                 */
                if (allebuchstaben[i] == buchstaben[z]) {
                    anzahl[z] += 1;
                }
            }
        }

        //Hier werden die Ergebnisse formatiert ausgegeben
        for (int i = 0; i < buchstaben.length; i++) {
            output += buchstaben[i] + ": " + anzahl[i] + " (" + prozentsatz(anzahl[i], allebuchstaben.length) + "%)\n";
        }
        return output;
    }


    public String leetspeakKonverter() {

        //Hier wird der Inhalt in das "Leetspeak Format" konvertiert
        // https://de.wikipedia.org/wiki/Leetspeak

        char ersetzen[] = {'a', 'e', 'o', 'l', 't', 'g', 's', 'b'};
        char ersatz[] = {'4', '3', '0', '1', '7', '6', '5', '8'};
        String ausgabe = inhalt;
        /**
         * Hier werden Nacheinander die Buchstaben aeoltgsb mit den jeweiligen Zahlen ersetzt.
         */
        for (int i = 0; i < ersetzen.length; i++) {
            ausgabe = replaceChar(ausgabe, ersetzen[i], ersatz[i]);
        }
        return ausgabe;
    }

    private String replaceChar(String input, char ersetzen, char ersatz) {

        //Hier können Buchstaben in einem Text ersetzt werden

        char buchstaben[] = input.toLowerCase().toCharArray();
        String output = "";
        for (int i = 0; i < buchstaben.length; i++) {

            /**
             * Wenn der Buchstabe gleich dem zu ersetzenden ist,
             * wird der Buchstabe überschrieben
             */

            if (buchstaben[i] == ersetzen) {
                buchstaben[i] = ersatz;
            }
            output += buchstaben[i];
        }
        return output;
    }



}
