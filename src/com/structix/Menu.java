package com.structix;

import java.util.Scanner;

/**
 * Created by structix on 15.05.16.
 */

/**
 * Menu Klasse:
 * Diese Klasse bestimmt das Verhalten des Menüs.
 * Hier werden die Funktionen hinter den Menüeinträgen bestimmt.
 */

public class Menu {

    //Objekte vom Menubuilder werden erzeugt und mit den Überschriften initialisiert.
    Menubuilder mbuilder = new Menubuilder("Textanalyse");
    Menubuilder statsmenu = new Menubuilder("Statistiken");
    Menubuilder formatmenu = new Menubuilder("Textformatierung");

    Scanner eingabe = new Scanner(System.in);
    //Objekt der Analyse Klasse wird definiert.
    //Es findet keine Deklaration statt, da dieses Objekt einen Dateipfad benötigt.
    Analyse an;

    //Zum testen kann dem Analyse Objekt ein fester Dateipfad mitgeliefert werden
    public void debug() {
        an = new Analyse("Test.txt");
    }


    //Hier wird ein Dateipfad abgefragt
    public void dateipfad() {
        System.out.println("Geben Sie einen Dateipfad an (relativ sowie absolut erlaubt): ");
        //Instanz wird mit dem eingegebenen Dateipfad erstellt
        an = new Analyse(eingabe.nextLine());
    }

    //Hier wird das Verhalten des Hauptmenüs beschrieben
    public boolean hauptmenu() {
        //Das Hauptmenü wird ausgeführt, solange exit nicht wahr ist
        boolean exit = false;
        int auswahl = 0;
        //Menü wird mit Einträgen befüllt und auf das Ergebnis "gewartet"
        //Die Zeichen für den Rahmen müssen auch angegeben werden
        auswahl = mbuilder.menuAnzeigen(new String[]{"Anzahl der Wörter", "Anzahl der verschiedenen Wörter", "Anzahl der Buchstaben", "Statistik Menü", "Textformatierung"}, "||");
        switch (auswahl) {
            case 1:
                ausgabe("Anzahl der Wörter: " + an.laenge());
                break;
            case 2:
                ausgabe("Verschiedene Wörter: " + an.verschiedenWoerter());
                break;
            case 3:
                ausgabe("Buchstaben (inklusive Leerzeichen): " + an.anzBuchstabenMLeerzeichen());
                ausgabe("Buchstaben (ohne Leerzeichen): " + an.anzBuchstabenOLeerzeichen());
                break;
            case 4:
                //Aufruf eines weiteren Menüs
                while (!statistikmenu());
                break;
            case 5:
                //Aufruf des Untermenüs Textformatierung
                while (!textformatmenu());
                break;
            default:
                exit = true;
                break; //Nicht zwingend notwendig
        }
        //Boolean wird zurückgegeben
        return exit;
    }

    //Statistikmenü
    public boolean statistikmenu() {
        boolean exit = false;
        int auswahl = 0;
        //Menü wird wieder mit Strings befüllt und der Rückgabe Wert der Variable auswahl zugeordnet
        auswahl = statsmenu.menuAnzeigen(new String[]{"Enthaltene Leerzeichen", "Häufigkeit der verschiedenen Wörter", "Häufigkeit aller Buchstaben", "Häufigkeit einzelner Buchstaben"}, "||");
        //Fallunterscheidung der Variable auswahl
        switch (auswahl) {
            case 1:
                ausgabe("Prozentsatz der enthaltenen Leerzeichen: " + an.stats_leerzeichen() + "%");
                break;
            case 2:
                ausgabe(an.haeufigkeit());
                break;
            case 3:
                System.out.println(an.hauefigkeitBuchstaben("abcdefghijklmnopqrstuvwxyz"));
                break;
            case 4:
                System.out.println("Bitte geben Sie alle Buchstaben ein, wonach gesucht werden soll (Bsp.: abcd): ");
                System.out.println(an.hauefigkeitBuchstaben(eingabe.nextLine()));
                break;
            default:
                exit = true;
                break; //Nicht zwingend notwendig
        }
        //Boolean wird zurückgegeben
        return exit;
    }

    public boolean textformatmenu() {
        boolean exit = false;
        int auswahl = 0;
        //Menü wird wieder mit Strings befüllt und der Rückgabe Wert der Variable auswahl zugeordnet
        auswahl = formatmenu.menuAnzeigen(new String[]{"Nach Wörtern pro Zeile trennen", "Nach Anzahl der Buchstaben pro Zeile trennen", "Anzahl der Buchstaben des längsten Wortes", "1337 Text Konverter"}, "||");
        //Fallunterscheidung der Variable auswahl
        switch (auswahl) {
            case 1:
                System.out.println("Geben Sie bitte die Anzahl der Wörter pro Zeile ein: ");
                System.out.println(an.textformatWoerter(eingabe.nextInt()));
                break;
            case 2:
                System.out.println("Geben Sie bitte die Anzahl der Zeichen pro Zeile ein: ");
                System.out.println(an.textFormatBuchstaben(eingabe.nextInt()));
                break;
            case 3:
                System.out.println("Buchstaben des längsten Wortes: " + an.laengstesWort());
                break;
            case 4:
                System.out.println(an.leetspeakKonverter());
                break;
            default:
                exit = true;
                break; //Nicht zwingend notwendig
        }
        //Boolean wird zurückgegeben
        return exit;
    }


    //Methode zur einfacheren Ausgabe
    private void ausgabe(String eing) {
        System.out.println(eing);
    }

}
