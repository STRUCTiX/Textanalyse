package com.structix;


/**
 * Main:
 * Diese Klasse ist für die Ausführung des gesamten Programms verantwortlich.
 * Hier werden die nötigen Objekte erzeugt, um das Menü auszuführen.
 * Falls im args Array Argumente enthalten sind, wird das CommandlineInterface gestartet.
 *
 */

/**
 * Bearbeitungszeit
 * 14.05.16: 13:04 - 14:08 Uhr
 * 15.05.16: 20:41 - 21:38 Uhr
 * 16.05.16: 20:47 - 21:44 Uhr
 * 17.05.16: 11:59 - 12:37 Uhr
 * 20.05.16: 11:18 - 12:32 Uhr
 * 24:05.16: 17:46 - 19:17 Uhr
 * 25.05.16: 10:47 - 11:32 Uhr
 * 26.05.16: 11:34 - 11:57 Uhr
 * 29.05.16: 16:24 - 17:23 Uhr
 */

public class Main {

    public static void main(String[] args) {
        CommandLineInterface cmd = new CommandLineInterface(args);

        if (!cmd.argTest()) {
            //Menüteil wird ausgeführt, wenn keine Argumente übergeben werden

            //Menü Objekt wird erzeugt
            Menu m = new Menu();
            //Auskommentieren, um einen Dateipfad einzugeben
            m.debug();
            //m.dateipfad();

            /**
             * Solange kein exit Befehl kommt, wird das Menü angezeigt.
             * Es ist kein Schleifenkörper notwendig, da die Methode Hauptmenu
             * schon einen Boolean Wert zurückgibt und damit bestimmt, ob die Bedingung wahr
             * bleibt oder nicht
             */

            while (!m.hauptmenu());
            //Nachricht vor dem Beenden des Programms
            System.out.println("Auf Wiedersehen!");
        } else {
            //Commandline Part
            cmd.ausfuehren();

        }
    }
}
