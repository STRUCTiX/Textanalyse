package com.structix;

import java.util.Scanner;

/**
 * Created by structix on 15.05.16.
 */

/**
 * Diese Klasse wird benötigt, um das Layout der Menüs zu generieren.
 * Damit muss man sich nurnoch um die Funktionen hinter dem Layout kümmern
 * und kann das Menü auch schnell erweitern oder umgestalten.
 */

public class Menubuilder {

    private String ueberschrift;
    private Scanner eingabe = new Scanner(System.in);

    public Menubuilder(String title) {
        ueberschrift = title;
    }

    public int menuAnzeigen(String items[], String randzeichen) {
        int maxlaenge = maximalwert(items);
        int spaces;
        String spacesString;
        String ausgabe = "";
        //---Trennstrich generieren---
        String trennstrich = randzeichen;
        for (int i = 0; i < maxlaenge; i++) {
            trennstrich += "-";
        }
        trennstrich += randzeichen;
        ausgabe += trennstrich + "\n";
        //---Trennstrich generieren---

        //---Überschrift---
        ausgabe += randzeichen + ueberschrift;
        spaces = maxlaenge - ueberschrift.length();
        spacesString = "";
        for (int i = 0; i <= spaces - 1; i++) {
            spacesString += " ";
        }
        ausgabe += spacesString + randzeichen + "\n";
        //---Überschrift---

        //---Menüeinträge---
        int eing = 0;
        for (int i = 0; i < items.length; i++) {
            int l = 0, itlaenge = items.length;
            while (itlaenge > 0) {
                l++;
                itlaenge /= 10;
            }
            spaces = (maxlaenge - 4 - l) - items[i].length();
            spacesString = "";
            for (int z = 0; z < spaces; z++) {
                spacesString += " ";
            }
            ausgabe += randzeichen + items[i] + ": (" +  (i + 1) + ")" + spacesString + randzeichen + "\n";
        }

        //---Exit Eintrag---
        int l = 0, itlaenge = items.length + 1;
        while (itlaenge > 0) {
            l++;
            itlaenge /= 10;
        }
        spaces = (maxlaenge - 4 - l) - "Exit".length();
        spacesString = "";
        for (int z = 0; z <= spaces; z++) {
            spacesString += " ";
        }
        ausgabe += randzeichen + "Exit (" + (items.length + 1) + ")" + spacesString + randzeichen + "\n";
        System.out.print(ausgabe);
        //---Exit Eintrag---
        //---Menüeinträge---

        //---Abfrage---
        do {
            System.out.print(randzeichen + "Eingabe: ");
            eing = eingabeInt();
        } while (eing <= 0 || eing > items.length + 1);
        System.out.println(trennstrich);
        //---Abfrage---
        return eing;
    }

    private String eingabeString() {
        return eingabe.nextLine();
    }

    private int eingabeInt() {
        int input = 0;
        try {
            input = eingabe.nextInt();
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            input = 0;
        }
        return input;
    }

    private int maximalwert(String items[]) {
        int itemlaenge[] = new int[items.length];
        for (int i = 0; i < items.length; i++) {
            itemlaenge[i] = items[i].length();
        }
        int tempint;
        for (int i = 1; i < itemlaenge.length; i++) {
            for (int z = 0; z < itemlaenge.length - i; z++) {
                if (itemlaenge[z] < itemlaenge[z + 1]) {
                    tempint = itemlaenge[z];
                    itemlaenge[z] = itemlaenge[z + 1];
                    itemlaenge[z + 1] = tempint;
                }
            }
        }
        int l = 0, itlaenge = items.length;
        while (itlaenge > 0) {
            l++;
            itlaenge /= 10;
        }
        //return itemlaenge[0] + 4 + (items.length / 10);
        return itemlaenge[0] + 4 + l;
    }

}
