package com.structix;

import java.util.Scanner;

/**
 * Created by structix on 24.05.16.
 */
public class CommandLineInterface {

    private String cmds[];
    private String cmdseinzeln[];
    private String kommandos[] = {"-h", "-?", "--help", "--credits", "-w", "-v", "-b", "-ha", "-sl", "-tw", "-tb",
            "-tl", "-bz", "-1337", "-m"};

    private String hilfe[] = {"Hilfe", "Hilfe", "Hilfe", "Credits", "Anzahl der Wörter",
            "Anzahl der verwschiedenen Wörter", "Anzahl der Buchstaben", "Häufigkeit der Wörter",
            "Anteil der Leerzeichen im Text", "Textformatierung nach Anzahl der Wörter pro Zeile",
            "Textformatierung nach Anzahl der Buchstaben pro Zeile", "Textformatierung: Längstes Wort",
            "Buchstaben zählen", "1337 Text Konverter", "Menü anzeigen"};

    private String DEVELOPER = "Janek Schoffit";
    private String VERSIONNAME = "1.0";


    String dateipfad = "";
    int dateipfadstelle = -1;

    public CommandLineInterface(String args[]) {
        cmds = args;
        verschiedenCmds();
    }

    public boolean argTest() {
        boolean rueckgabe = false;
        if (cmds.length > 0) {
            rueckgabe = true;
        }
        return rueckgabe;
    }

    private boolean gueltigkeit() {
        int i = 0, dateizaehler = 0;
        boolean ungueltig = false, vorhanden;
        while (i < cmdseinzeln.length && !ungueltig) {
            if (cmdseinzeln[i].substring(0, 1).equals("-") && cmdseinzeln[i].length() > 1) {
                vorhanden = false;
                for (int z = 0; z < kommandos.length; z++) {
                    if (cmdseinzeln[i].equals(kommandos[z])) {
                        vorhanden = true;
                    }
                }
                if (!vorhanden) {
                    ungueltig = true;
                }
            } else {
                if (cmdseinzeln[i].matches("[a-zA-Z0-9]+.[a-zA-Z]{3}")) {
                    dateipfad = cmdseinzeln[i];
                    dateizaehler++;
                    if (dateizaehler > 1) {
                        ungueltig = true;
                    }
                }
            }

            i++;
        }

        return !ungueltig;
    }

    public void ausfuehren() {
        if (gueltigkeit()) {
            Scanner eingabe = new Scanner(System.in);
            String datei = dateipfad;
            if (datei.isEmpty()) {
                System.out.println("Geben Sie einen Dateipfad an (relativ sowie absolut erlaubt): ");
                datei = eingabe.nextLine();
            }
            Analyse an = new Analyse(datei);

            for (int i = 0; i < cmdseinzeln.length; i++) {
                if (i != dateipfadstelle) {
                    switch (cmdseinzeln[i]) {
                        case "-h":
                        case "-?":
                        case "--help":
                            System.out.println(hilfe());
                            break;
                        case "--credits":
                            System.out.println(credits());
                            break;
                        case "-w":
                            System.out.println("Anzahl der Wörter: " + an.laenge());
                            break;
                        case "-v":
                            System.out.println("Anzahl der verschiedenen Wörter: " + an.verschiedenWoerter());
                            break;
                        case "-b":
                            System.out.println("Anzahl der Buchstaben (inklusive Leerzeichen): " + an.anzBuchstabenMLeerzeichen());
                            System.out.println("Anzahl der Buchstaben (ohne Leerzeichen): " + an.anzBuchstabenOLeerzeichen());
                            break;
                        case "-ha":
                            System.out.println(an.haeufigkeit());
                            break;
                        case "-sl":
                            System.out.println("Prozentsatz der enthaltenen Leerzeichen: " + an.stats_leerzeichen() + "%");
                            break;
                        case "-tw":
                            System.out.println("Geben Sie bitte die Anzahl der Wörter pro Zeile ein: ");
                            System.out.println(an.textformatWoerter(eingabe.nextInt()));
                            break;
                        case "-tb":
                            System.out.println("Geben Sie bitte die Anzahl der Zeichen pro Zeile ein: ");
                            System.out.println(an.textFormatBuchstaben(eingabe.nextInt()));
                            break;
                        case "-tl":
                            System.out.println("Buchstaben des längsten Wortes: " + an.laengstesWort());
                            break;
                        case "-bz":
                            System.out.println("Bitte geben Sie alle Buchstaben ein, wonach gesucht werden soll (Bsp.: abcd): ");
                            System.out.println(an.hauefigkeitBuchstaben(eingabe.nextLine()));
                            break;
                        case "-1337":
                            System.out.println(an.leetspeakKonverter());
                            break;
                        case "-m":
                            //Menü Objekt wird erzeugt
                            Menu m = new Menu();
                            m.dateipfad();
                            //Hauptmenü anzeigen
                            while (!m.hauptmenu());
                            break;
                    }
                }
            }
        } else {
            System.out.println("Ungültige Argumente");
        }
    }


    private String hilfe() {
        String hilfeText = "Benutzung: Textanalyse <Dateipfad>\n";
        for (int i = 0; i < kommandos.length; i++) {
            hilfeText += kommandos[i] + " || " + hilfe[i] + "\n";
        }
        return hilfeText;
    }

    private String credits() {
        return "Entwickler: " + DEVELOPER + "\nVersion: " + VERSIONNAME;
    }



    private void verschiedenCmds() {
        String tempCmds[] = new String[cmds.length];
        boolean vorhanden;
        int tbelegt = 0;
        for (String cmd : cmds) {
            vorhanden = false;
            for (int z = 0; z < tempCmds.length; z++) {
                if (cmd.equals(tempCmds[z])) {
                    vorhanden = true;
                }
            }
            if (!vorhanden) {
                tempCmds[tbelegt] = cmd;
                tbelegt++;
            }
        }
        cmdseinzeln = new String[tbelegt];
        for (int i = 0; i < tbelegt; i++) {
            cmdseinzeln[i] = tempCmds[i];

        }
    }





}
